/********************************************************************************
** Form generated from reading UI file 'qt_project.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QT_PROJECT_H
#define UI_QT_PROJECT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Qt_projectClass
{
public:
    QPushButton *pushButton;

    void setupUi(QDialog *Qt_projectClass)
    {
        if (Qt_projectClass->objectName().isEmpty())
            Qt_projectClass->setObjectName(QStringLiteral("Qt_projectClass"));
        Qt_projectClass->resize(600, 400);
        pushButton = new QPushButton(Qt_projectClass);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(134, 200, 181, 141));

        retranslateUi(Qt_projectClass);
        QObject::connect(pushButton, SIGNAL(clicked()), Qt_projectClass, SLOT(slot1()));

        QMetaObject::connectSlotsByName(Qt_projectClass);
    } // setupUi

    void retranslateUi(QDialog *Qt_projectClass)
    {
        Qt_projectClass->setWindowTitle(QApplication::translate("Qt_projectClass", "Qt_project", 0));
        pushButton->setText(QApplication::translate("Qt_projectClass", "Gombik", 0));
    } // retranslateUi

};

namespace Ui {
    class Qt_projectClass: public Ui_Qt_projectClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QT_PROJECT_H
