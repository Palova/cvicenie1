#ifndef QT_PROJECT_H
#define QT_PROJECT_H

#include <QtWidgets/QDialog>
#include "ui_qt_project.h"
#include<QMessageBox>

class Qt_project : public QDialog
{
	Q_OBJECT

public:
	Qt_project(QWidget *parent = 0);
	~Qt_project();

	public slots:
	void slot1(); 

private:
	Ui::Qt_projectClass ui;
	QMessageBox msgBox;
};

#endif // QT_PROJECT_H
